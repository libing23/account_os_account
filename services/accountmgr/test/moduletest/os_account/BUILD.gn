# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/test.gni")
import("../../../../../os_account.gni")

module_output_path = "os_account/services/os_account"

config("os_account_manager_service_test_config") {
  include_dirs = [
    "${services_path}/accountmgr/test/mock/common",
    "${services_path}/accountmgr/test/mock/os_account",
    "${services_path}/accountmgr/include",
    "${services_path}/accountmgr/include/osaccount",
    "${common_path}/log/include",
    "${common_path}/perf_stat/include",
    "${common_path}/account_error/include",
    "${innerkits_path}/include",
    "//third_party/json/include",
  ]
}

ohos_moduletest("inner_os_account_manager_test") {
  module_out_path = module_output_path

  sources = [ "inner_os_account_manager_test.cpp" ]

  configs = [ "${services_path}/accountmgr/test:accountmgr_test_config" ]

  include_dirs = [
    "${services_path}/accountmgr/include",
    "${services_path}/accountmgr/include/osaccount",
    "${common_path}/log/include",
    "${common_path}/perf_stat/include",
    "${common_path}/account_error/include",
    "${innerkits_path}/include",
  ]

  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gtest_main",
  ]
  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

ohos_moduletest("os_account_manager_service_module_test") {
  module_out_path = module_output_path

  sources = [ "os_account_manager_service_module_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/common/bundle_manager_adapter.cpp",
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]
  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:base",
    "ability_base:want",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]

  if (os_account_enable_multiple_os_accounts || use_clang_coverage) {
    cflags = [ "-DENABLE_MULTIPLE_OS_ACCOUNTS" ]
  }
  part_name = "os_account"
}

config("os_account_manager_service_create_os_account_test_config") {
  include_dirs = [
    "${services_path}/accountmgr/test/mock/os_account",
    "${services_path}/accountmgr/include",
    "${services_path}/accountmgr/include/osaccount",
    "${common_path}/log/include",
    "${common_path}/perf_stat/include",
    "${common_path}/account_error/include",
    "${innerkits_path}/include",
    "//third_party/json/include",
  ]
}

ohos_moduletest("os_account_manager_service_create_os_account_test") {
  module_out_path = module_output_path

  sources = [ "os_account_manager_service_create_os_account_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/common/bundle_manager_adapter.cpp",
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_create_os_account_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]
  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

config("os_account_manager_service_subscribe_test_config") {
  include_dirs = [
    "${services_path}/accountmgr/test/mock/os_account",
    "${services_path}/accountmgr/include",
    "${services_path}/accountmgr/include/osaccount",
    "${common_path}/log/include",
    "${common_path}/perf_stat/include",
    "${common_path}/account_error/include",
    "${innerkits_path}/include",
    "${account_iam_interfaces_native_path}/include",
    "${account_iam_framework_path}/test/unittest/include",
    "//third_party/json/include",
  ]
}

ohos_moduletest("os_account_manager_service_subscribe_module_test") {
  module_out_path = module_output_path

  sources = [ "os_account_manager_service_subscribe_module_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/common/bundle_manager_adapter.cpp",
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_subscribe_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]

  deps = [
    "${account_iam_framework_path}:account_iam_innerkits",
    "${account_iam_framework_path}/test/unittest:account_iam_client_test",
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]
  external_deps = [
    "ability_base:base",
    "ability_base:want",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "access_token:libaccesstoken_sdk",
    "access_token:libtoken_setproc",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"

  cflags_cc = []

  if (has_pin_auth_part) {
    cflags_cc += [ "-DHAS_PIN_AUTH_PART" ]
    external_deps += [ "pin_auth:pinauth_framework" ]
  }

  if (has_user_auth_part) {
    cflags_cc += [ "-DHAS_USER_AUTH_PART" ]
    external_deps += [ "user_auth_framework:userauth_client" ]
  }
}

ohos_moduletest("os_account_control_file_manager_test") {
  module_out_path = module_output_path

  sources = [ "os_account_control_file_manager_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_subscribe_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]

  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]
  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

ohos_moduletest("os_account_database_operator_test") {
  module_out_path = module_output_path

  sources = [ "os_account_database_operator_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_subscribe_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]

  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]
  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

ohos_moduletest("os_account_file_operator_test") {
  module_out_path = module_output_path

  sources = [ "os_account_file_operator_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_manager_service_subscribe_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]

  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]
  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

config("os_account_photo_operator_test_config") {
  include_dirs = [
    "${services_path}/accountmgr/test/mock/common",
    "${services_path}/accountmgr/test/mock/os_account",
    "${services_path}/accountmgr/include",
    "${services_path}/accountmgr/include/osaccount",
    "${common_path}/log/include",
    "${common_path}/perf_stat/include",
    "${common_path}/account_error/include",
    "${innerkits_path}/include",
    "//third_party/json/include",
  ]
}

ohos_moduletest("os_account_photo_operator_test") {
  module_out_path = module_output_path

  sources = [ "os_account_photo_operator_test.cpp" ]
  sources += [
    "${services_path}/accountmgr/src/osaccount/os_account_photo_operator.cpp",
    "${services_path}/accountmgr/test/mock/os_account/os_account_interface.cpp",
    "${services_path}/accountmgr/test/mock/os_account/permission_kit.cpp",
  ]
  configs = [
    ":os_account_photo_operator_test_config",
    "${services_path}/accountmgr/test:accountmgr_test_config",
  ]
  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "${services_path}/accountmgr:accountmgr",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "kv_store:distributeddata_inner",
  ]
  part_name = "os_account"
}

group("moduletest") {
  testonly = true

  deps = [
    ":inner_os_account_manager_test",
    ":os_account_control_file_manager_test",
    ":os_account_database_operator_test",
    ":os_account_file_operator_test",
    ":os_account_manager_service_module_test",
    ":os_account_photo_operator_test",
  ]
  if (os_account_enable_multiple_os_accounts || use_clang_coverage) {
    deps += [ ":os_account_manager_service_subscribe_module_test" ]
  }
}
