# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("../../../../../services/accountmgr/os_account_service.gni")

module_output_path = "os_account/frameworks/os_account"

config("os_account_frameworks_config_unittest") {
  visibility = [ ":*" ]

  include_dirs = [
    "${innerkits_path}/include",
    "${innerkits_native_path}/include",
  ]

  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }

  defines = [
    "ACCOUNT_LOG_TAG = \"OsAccountMTestFwk\"",
    "LOG_DOMAIN = 0xD001B00",
  ]
}

ohos_moduletest("os_account_frameworks_module_test") {
  module_out_path = module_output_path

  sources = [
    "${innerkits_native_path}/src/account_info_parcel.cpp",
    "${innerkits_native_path}/src/account_proxy.cpp",
    "os_account_manager_module_test.cpp",
  ]
  configs = [ ":os_account_frameworks_config_unittest" ]

  deps = [
    "${common_path}:libaccount_common",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "access_token:libaccesstoken_sdk",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "hilog:libhilog",
    "init:libbegetutil",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_moduletest("os_account_frameworks_module_mock_test") {
  module_out_path = module_output_path
  include_dirs = [
    "${os_account_dfx_path}/hidumper_adapter",
    "${services_path}/accountmgr/include/bundle_manager_adapter",
    "${services_path}/accountmgr/include/ability_manager_adapter",
    "${services_path}/accountmgr/include",
    "${os_account_dfx_path}/hitrace_adapter",
    "${services_path}/accountmgr/include/account_iam",
    "${services_path}/accountmgr/include/domain_account",
    "${services_path}/accountmgr/include/osaccount",
    "${bundle_framework_path}/interfaces/inner_api/appexecfwk_core/include",
    "${os_account_path}/interfaces/innerkits/ohosaccount/native/include/",
  ]
  sources = [
    "${app_account_services_path}/test/mock/common/ability_manager_adapter_mock.cpp",
    "${app_account_services_path}/test/mock/common/bundle_manager_adapter.cpp",
    "${common_path}/log/src/account_log_wrapper.cpp",
    "${innerkits_native_path}/src/account_info_parcel.cpp",
    "${innerkits_native_path}/src/account_proxy.cpp",
    "${innerkits_native_path}/src/ohos_account_kits.cpp",
    "${innerkits_native_path}/src/ohos_account_kits_impl.cpp",
    "${os_account_dfx_path}/hidumper_adapter/account_dump_helper.cpp",
    "${os_account_dfx_path}/hitrace_adapter/hitrace_adapter.cpp",
    "${services_path}/accountmgr/src/account_iam/account_iam_callback.cpp",
    "${services_path}/accountmgr/src/account_iam/account_iam_mgr_stub.cpp",
    "${services_path}/accountmgr/src/account_iam/account_iam_service.cpp",
    "${services_path}/accountmgr/src/account_iam/inner_account_iam_manager.cpp",
    "os_account_manager_module_test.cpp",
  ]
  sources += account_service_sources
  configs = [ ":os_account_frameworks_config_unittest" ]

  deps = [
    "${account_iam_framework_path}:account_iam_innerkits",
    "${common_path}:libaccount_common",
    "${domain_account_framework_path}:domain_account_innerkits",
    "${innerkits_native_path}:libaccountkits",
    "${os_account_innerkits_native_path}:os_account_innerkits",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
    "//third_party/mbedtls:mbedtls",
  ]

  use_exceptions = true

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libtoken_setproc",
    "access_token:libtokenid_sdk",
    "bundle_framework:appexecfwk_base",
    "c_utils:utils",
    "hilog:libhilog",
    "huks:libhukssdk",
    "init:libbegetutil",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
  cflags_cc = []
  if (has_user_auth_part) {
    cflags_cc += [ "-DHAS_USER_AUTH_PART" ]
    external_deps += [ "user_auth_framework:userauth_client" ]
  }
  if (has_ces_part) {
    cflags_cc += [ "-DHAS_CES_PART" ]
    external_deps += [ "common_event_service:cesfwk_innerkits" ]
  }
  cflags_cc += [ "-DBUNDLE_ADAPTER_MOCK" ]
  cflags_cc += [ "-DENABLE_MULTIPLE_OS_ACCOUNTS" ]
  part_name = "os_account"
}

group("moduletest") {
  testonly = true

  deps = [ ":os_account_frameworks_module_mock_test" ]
}
